package com.mp;

import com.mp.dao.UserMapper;
import com.mp.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: InsertTest
 * @Description:
 * @date: 2022/2/25 16:43
 * @since JDK 1.8
 */
@SpringBootTest
public class IDTypeTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
    全局策略
    yml文件中设置主键策略
        global-config:
          db-config:
            id-type: uuid
     如果有局部策略，则优先执行局部策略，全局策略不生效
        如：@TableId(type = IdType.ID_WORKER_STR)
     */
    @Test
    public void insert5() {
        User user = new User();
//        id不为空的情况下会插入设置的id
//        user.setUserId("dadadad54465");
        user.setRealName("钱大忘5");
        user.setAge(24);
        user.setManagerId(1088248166370832385L);
        user.setEmail("qdw5@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        boolean insert = user.insert();
        System.out.println(insert);
        System.out.println("主键："+ user.getUserId());
    }

    /*
    主键策略
    ID_WORKER_STR类型，,id为空才会自动填充
    基于雪花算法的字符串类型
    @TableId(type = IdType.ID_WORKER_STR)
     */
    @Test
    public void insert4() {
        User user = new User();
//        id不为空的情况下会插入设置的id
//        user.setUserId("dadadad54465");
        user.setRealName("钱大忘4");
        user.setAge(24);
        user.setManagerId(1088248166370832385L);
        user.setEmail("qdw4@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        boolean insert = user.insert();
        System.out.println(insert);
        System.out.println("主键："+ user.getUserId());
    }

    /*
    主键策略
    UUID类型
    字符串类型,id为空才会自动填充
    @TableId(type = IdType.UUID)
     */
    @Test
    public void insert3() {
        User user = new User();
        user.setRealName("钱大忘");
        user.setAge(24);
        user.setManagerId(1088248166370832385L);
        user.setEmail("qdw@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        boolean insert = user.insert();
        System.out.println(insert);
        System.out.println("主键："+ user.getUserId());
    }

    /*
    主键策略
    未设置主键类型
    默认跟随全局策略，如果全局策略不改变，默认雪花算法，
    设置id会插入设置的id，没设置id则插入自动生成的id
    @TableId(type = IdType.NONE)
     */
    @Test
    public void insert2() {
        User user = new User();
//        插入设置的id
//        user.setUserId(6546465464646465L);
        user.setRealName("钱大福2");
        user.setAge(24);
        user.setManagerId(1088248166370832385L);
        user.setEmail("qdf2@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        boolean insert = user.insert();
        System.out.println(insert);
        System.out.println("主键："+ user.getUserId());
    }

    /*
    主键策略
    数据库ID自增
    @TableId(type = IdType.AUTO) //数据库自增
     */
    @Test
    public void insert() {
        User user = new User();
        user.setRealName("钱福");
        user.setAge(24);
        user.setManagerId(1088248166370832385L);
        user.setEmail("zc@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        boolean insert = user.insert();
        System.out.println(insert);
        System.out.println("主键："+ user.getUserId());
    }
}
