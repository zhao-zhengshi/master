package com.mp;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mp.dao.UserMapper;
import com.mp.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: InsertTest
 * @Description:
 * @date: 2022/2/25 16:43
 * @since JDK 1.8
 */
@SpringBootTest
public class deleteTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
    删除方法
    通过lambda删除数据
     */
    @Test
    public void deleteByWrapper() {
        LambdaQueryWrapper<User> lambdaQueryWrapper = Wrappers.<User>lambdaQuery();
        lambdaQueryWrapper.eq(User::getAge, 28).or().gt(User::getAge,41);
        int rows = userMapper.delete(lambdaQueryWrapper);
        System.out.println("删除条数："+rows);
    }

    /*
    删除方法
    通过id集合删除数据
     */
    @Test
    public void deleteBatchIds() {
        int rows = userMapper.deleteBatchIds(Arrays.asList(1497137621670793218L,1497137036825452545L,1497135567128723457L));
        System.out.println("删除条数："+rows);
    }

    /*
   删除方法
   通过map删除数据
    */
    @Test
    public void deleteByMap() {
        HashMap<String, Object> columnMap = new HashMap<>();
        columnMap.put("name","向前");
        columnMap.put("age",28);
        int rows = userMapper.deleteByMap(columnMap);
        System.out.println("删除条数："+rows);
    }

    /*
    删除方法
    通过id删除数据
     */
    @Test
    public void deleteById() {
        int rows = userMapper.deleteById(1497135643691491330L);
        System.out.println("删除条数："+rows);
    }
}
