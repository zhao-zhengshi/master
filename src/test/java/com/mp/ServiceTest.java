package com.mp;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mp.entity.User;
import com.mp.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: InsertTest
 * @Description:
 * @date: 2022/2/25 16:43
 * @since JDK 1.8
 */
@SpringBootTest
public class ServiceTest {

    @Autowired(required = false)
    private UserService userService;


    /*
      通用Service
      删除
   */
    @Test
    public void chain3() {
        boolean updateBoolean = userService.lambdaUpdate().eq(User::getRealName,"徐丽2").remove();
        System.out.println(updateBoolean);
    }

    /*
      通用Service
      更新
   */
    @Test
    public void chain2() {
        boolean updateBoolean = userService.lambdaUpdate().gt(User::getAge, 25).set(User::getAge, 26).update();
        System.out.println(updateBoolean);
    }

    /*
   通用Service
   查询
    */
    @Test
    public void chain() {
        List<User> list = userService.lambdaQuery().gt(User::getAge, 25).like(User::getRealName, "雨").list();
        list.forEach(System.out::println);
    }

    /*
    通用Service
    批量插入
     */
    @Test
    public void batch() {
        User user1 = new User();
        user1.setRealName("徐丽1");
        user1.setAge(26);
        user1.setManagerId(1088248166370832385L);
        user1.setEmail("xl1@baomidou.com");
        user1.setCreateTime(LocalDateTime.now());
        user1.setRemark("我是一个备注哦！");

        User user2 = new User();
//        有id会进行更新
        user2.setUserId("a91c1c1e0d022245fea9177b25076c68");
        user2.setRealName("徐丽2");
        user2.setAge(26);
        user2.setManagerId(1088248166370832385L);
        user2.setEmail("xl2@baomidou.com");
        user2.setCreateTime(LocalDateTime.now());
        user2.setRemark("我是一个备注哦！");

        List<User> userList = Arrays.asList(user1, user2);
//        boolean saveBatch = userService.saveBatch(userList);
        boolean saveBatch = userService.saveOrUpdateBatch(userList);
        System.out.println(saveBatch);
    }

    /*
    通用Service
    查询一个
     */
    @Test
    public void getOne() {
        User one = userService.getOne(Wrappers.<User>lambdaQuery().gt(User::getAge, 25),false);
        System.out.println(one);
    }
}
