package com.mp;

import com.mp.dao.UserMapper;
import com.mp.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: InsertTest
 * @Description:
 * @date: 2022/2/25 16:43
 * @since JDK 1.8
 */
@SpringBootTest
public class InsertTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    @Test
    public void insert() {
        User user = new User();
        user.setRealName("向前");
        user.setAge(26);
        user.setManagerId(1088248166370832385L);
        user.setEmail("xq@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        int rows = userMapper.insert(user);
        System.out.print("影响记录数："+rows);
    }
}
