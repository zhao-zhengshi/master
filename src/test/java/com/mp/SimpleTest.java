package com.mp;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import com.mp.dao.UserMapper;
import com.mp.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class SimpleTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
       分页查询
       自定义查询
     */
    @Test
    public void selectMyPage(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge("age", 26);

        Page<User> page = new Page<User>(1, 2);

        IPage<User> userIPage = userMapper.selectUserPage(page, queryWrapper);

        System.out.println("总页数：" + userIPage.getPages());
        System.out.println("总记录数：" + userIPage.getTotal());
        List<User> userList = userIPage.getRecords();
    }

    /*
       分页查询
     */
    @Test
    public void selectPage(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.ge("age", 26);
//只会查询分页的数据，不会查询分页后的总数
        Page<User> page = new Page<User>(1, 2, false);
//        Page<User> page = new Page<User>(1, 2);

        IPage<User> userIPage = userMapper.selectPage(page, queryWrapper);

        /*System.out.println("总页数：" + userIPage.getPages());
        System.out.println("总记录数：" + userIPage.getTotal());
        List<User> userList = userIPage.getRecords();*/

        IPage<Map<String, Object>> mapIPage = userMapper.selectMapsPage(page, queryWrapper);
        System.out.println("总记录数：" + userIPage.getTotal());
        List<Map<String, Object>> userList = mapIPage.getRecords();
        userList.forEach(System.out::println);
    }

    /*
       自定义sql
       名字为王姓，并且（年龄小于40或邮箱不为空）
     */
    @Test
    public void selectMy(){
//        LambdaQueryWrapper<User> lambda = new QueryWrapper<User>().lambda();
//        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<User> lambdaQuery = Wrappers.<User>lambdaQuery();
        lambdaQuery.likeRight(User::getRealName,"王").and(lqw -> lqw.lt(User::getAge, 40).or()
                .isNotNull(User::getEmail));
        List<User> userList = userMapper.selectAll(lambdaQuery);
        userList.forEach(System.out::println);
    }

    /*
       Lambda条件构造器
       LambdaQueryChainWrapper
     */
    @Test
    public void selectLambda3(){
        List<User> userList = new LambdaQueryChainWrapper<User>(userMapper)
                .like(User::getRealName, "雨").ge(User::getAge, 20).list();
        userList.forEach(System.out::println);
    }

    /*
       Lambda条件构造器
       名字为王姓，并且（年龄小于40或邮箱不为空）
     */
    @Test
    public void selectLambda2(){
//        LambdaQueryWrapper<User> lambda = new QueryWrapper<User>().lambda();
//        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<User> lambdaQuery = Wrappers.<User>lambdaQuery();
        lambdaQuery.likeRight(User::getRealName,"王").and(lqw -> lqw.lt(User::getAge, 40).or()
                .isNotNull(User::getEmail));
        List<User> userList = userMapper.selectList(lambdaQuery);
        userList.forEach(System.out::println);
    }

    /*
       Lambda条件构造器
     */
    @Test
    public void selectLambda(){
//        LambdaQueryWrapper<User> lambda = new QueryWrapper<User>().lambda();
//        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<User> lambdaQuery = Wrappers.<User>lambdaQuery();
        lambdaQuery.like(User::getRealName,"雨").lt(User::getAge, 40);
        List<User> userList = userMapper.selectList(lambdaQuery);
        userList.forEach(System.out::println);
    }

    /*
       其它使用条件构造器的方法
       查询返回一条，多条会报错
     */
    @Test
    public void selectByWrappeOne(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.like("name","刘雨红").lt("age", 40);

        User user = userMapper.selectOne(queryWrapper);
        System.out.print(user);
    }

    /*
       其它使用条件构造器的方法
       查询记录数
     */
    @Test
    public void selectByWrappeCount(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.like("name","雨").lt("age", 40);

        Integer count = userMapper.selectCount(queryWrapper);
        System.out.print("总记录数：" +count);
    }

    /*
       其它使用条件构造器的方法
       只返回第一列
     */
    @Test
    public void selectByWrappeObjs(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.select("user_id", "name").like("name","雨").lt("age", 40);

        List<Object> userList = userMapper.selectObjs(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
       其它使用条件构造器的方法
       按照直属上级分组，查询每组的平均年龄、最大年龄、最小年龄，并且只取年龄总和小于500的组
     */
    @Test
    public void selectByWrappeMaps2(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.select("avg(age) avg_age", "min(age) min_age", "max(age) max_age")
                .groupBy("manager_id").having("sum(age)<{0}", 500);

        List<Map<String, Object>> userList = userMapper.selectMaps(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
       其它使用条件构造器的方法
       返回map
     */
    @Test
    public void selectByWrappeMaps(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
//  只取选定的列
        queryWrapper.select("user_id", "name").like("name","雨").lt("age", 40);

        List<Map<String, Object>> userList = userMapper.selectMaps(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
       AllEq用法
     */
    @Test
    public void selectByWrappeAllEq(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", "王天风");
        params.put("age", null);
        // false如果值为null则不查询
//        queryWrapper.allEq(params,false);
        queryWrapper.allEq((k,v) -> !k.equals("name"),params);

        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
       实体作为条件构造器构造方法的参数
     */
    @Test
    public void selectByWrappeEntity(){
        User whereUser = new User();
        whereUser.setRealName("刘红雨");
        whereUser.setAge(32);

        QueryWrapper<User> queryWrapper = new QueryWrapper<User>(whereUser);
        queryWrapper.like("name","雨").lt("age",40);

        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    condition的作用
     */
    @Test
    public void testCondition(){
        String name = "王";
        String email = "";
        condition(name, email);
    }

    /*
    condition的作用
     */
    private void condition(String name,String email){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.like(StringUtils.isNotEmpty(name),"name", name)
                .like(StringUtils.isNotEmpty(email), "email", email);
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
       不列出全部字段
       年龄为30，31，34，35,查询不包含指定列
     */
    @Test
    public void selectByWrappeSupper2(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.in("age",Arrays.asList(30,31,34,35))
                .select(User.class,info -> !info.getColumn().equals("create_time") &&
                        !info.getColumn().equals("manager_id"));
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
       不列出全部字段
       年龄为30，31，34，35,查询指定列
     */
    @Test
    public void selectByWrappeSupper(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.select("user_id","name").in("age",Arrays.asList(30,31,34,35));
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
    年龄为30，31，34，35
    */
    @Test
    public void selectByWrappe8(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        //  .last("limit 1")有sql注入危险
        queryWrapper.in("age",Arrays.asList(30,31,34,35)).last("limit 1");
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
  （年龄小于40或邮箱不为空）并且名字为王姓
   */
    @Test
    public void selectByWrappe7(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.likeRight("name","王").
                nested(wq -> wq.lt("age","40").or().isNotNull("email"));
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
   名字为王姓或者（年龄小于40并且年龄大于20并且邮箱不为空）
    */
    @Test
    public void selectByWrappe6(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.likeRight("name","王").
                or(wq -> wq.lt("age","40").gt("age",20).isNotNull("email"));
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
   名字为王姓并且（年龄小于40或邮箱不为空）
    */
    @Test
    public void selectByWrapper5(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.likeRight("name","王").and(wq -> wq.lt("age","40").or().isNotNull("email"));
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
   创建日期为2019年2月14日并且直属上级名字为王姓
    */
    @Test
    public void selectByWrapper4(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.apply("date_format(create_time,'%Y-%m-%d') = {0}","2019-02-14")
                .inSql("manager_id", "select user_id from mp_user where name like '王%'");
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
   名字为王姓或者年龄大于等于25，按照年龄降序排列，年龄相同按照id升序排列
    */
    @Test
    public void selectByWrapper3(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.likeRight("name","王").or().ge("age",25).orderByDesc("age")
                .orderByAsc("user_id");
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
    名字中包含雨且年龄大于等于20且小于等于40，并且email不为空
    */
    @Test
    public void selectByWrapper2(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
        queryWrapper.like("name","雨").between("age",20,40).isNotNull("email");
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    条件构造器查询
    名字中包含雨且年龄小于40
     */
    @Test
    public void selectByWrapper(){
        QueryWrapper<User> queryWrapper = new QueryWrapper<User>();
//        QueryWrapper<User> queryWrapper1 = Wrappers.<User>query();
        queryWrapper.like("name","雨").lt("age",40);
        List<User> userList = userMapper.selectList(queryWrapper);
        userList.forEach(System.out::println);
    }

    /*
    普通查询
     */
    @Test
    public void selectByMap(){
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("manager_id", 1088248166370832385L);
//        map.put("name", "王天风");
//        map.put("age", 25);
        List<User> userLists = userMapper.selectByMap(map);
        userLists.forEach(System.out::println);
    }

    /*
    普通查询
     */
    @Test
    public void selectByIds(){
        List<Long> ids = Arrays.asList(1088248166370832385L, 1088250446457389058L);
        List<User> userLists = userMapper.selectBatchIds(ids);
        userLists.forEach(System.out::println);
    }

    /*
    普通查询
     */
    @Test
    public void selectById(){
        User user = userMapper.selectById(1094592041087729666L);
        System.out.print(user);
    }

    /*
    普通查询
     */
    @Test
    void contextLoads() {
        List<User> list = userMapper.selectList(null);
        list.forEach(System.out::println);
    }

}
