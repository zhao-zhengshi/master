package com.mp;

import com.mp.dao.UserMapper;
import com.mp.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: InsertTest
 * @Description:
 * @date: 2022/2/25 16:43
 * @since JDK 1.8
 */
@SpringBootTest
public class ARTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
    AR模式
    插入或者更新数据
     */
    @Test
    public void insertOrUpdate() {
        User user = new User();
//        id存在则进行更新操作
//        user.setUserId(1498475178505531393L);
        user.setRealName("张强");
        user.setAge(24);
        user.setManagerId(1088248166370832385L);
        user.setEmail("zc@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        boolean insertOrUpdate = user.insertOrUpdate();
        System.out.println(insertOrUpdate);
    }

    /*
    AR模式
    通过id删除数据
     */
    @Test
    public void deleteById() {
        User user = new User();
//        user.setUserId(1498472001374744578L);
        boolean deleteById = user.deleteById();
        System.out.println(deleteById);
    }

    /*
    AR模式
    通过id更新数据
     */
    @Test
    public void updateById() {
        User user = new User();
//        user.setUserId(1498472001374744578L);
        user.setRealName("张曹操");
        boolean updateById = user.updateById();
        System.out.println(updateById);
    }

    /*
    AR模式
    通过实体设置id，不传参数查询
     */
    @Test
    public void selectById2() {
        User user = new User();
//        user.setUserId(1498472001374744578L);
        User selectUser = user.selectById();
        System.out.println(selectUser == user);
        System.out.println(selectUser);
    }

    /*
    AR模式
    通过id查询
     */
    @Test
    public void selectById() {
        User user = new User();
        User selectUser = user.selectById(1498472001374744578L);
        System.out.println(selectUser == user);
        System.out.println(selectUser);
    }

    /*
    AR模式(通过实体类对数据库的表数据进行操作)
    插入数据
     */
    @Test
    public void insert() {
        User user = new User();
        user.setRealName("张草");
        user.setAge(24);
        user.setManagerId(1088248166370832385L);
        user.setEmail("zc@baomidou.com");
        user.setCreateTime(LocalDateTime.now());
        user.setRemark("我是一个备注哦！");
        boolean insert = user.insert();
        System.out.println(insert);
    }
}
