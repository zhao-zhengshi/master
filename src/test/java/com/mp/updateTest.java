package com.mp;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.service.additional.update.impl.LambdaUpdateChainWrapper;
import com.mp.dao.UserMapper;
import com.mp.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: InsertTest
 * @Description:
 * @date: 2022/2/25 16:43
 * @since JDK 1.8
 */
@SpringBootTest
public class updateTest {

    @Autowired(required = false)
    private UserMapper userMapper;

    /*
   更新方法
   LambdaUpdateChainWrapper更新数据
    */
    @Test
    public void updateByLambda2() {
        boolean updateBoolean = new LambdaUpdateChainWrapper<User>(userMapper).eq(User::getRealName, "向东").eq(User::getAge, "26")
                .set(User::getEmail, "xd@@baomidou.com").update();
        System.out.println("影响是否成功："+updateBoolean);
    }

    /*
   更新方法
   lambdaWrapper,传入实体类为null，更新数据
    */
    @Test
    public void updateByLambda() {
        LambdaUpdateWrapper<User> lambdaUpdateWrapper = Wrappers.<User>lambdaUpdate();
        lambdaUpdateWrapper.eq(User::getRealName, "向西").eq(User::getAge, "30")
                .set(User::getEmail,"xx@@baomidou.com");

        int rows = userMapper.update(null,lambdaUpdateWrapper);
        System.out.println("影响记录数："+rows);
    }

    /*
   更新方法
   wrapper,传入实体类为null，更新数据
    */
    @Test
    public void updateByWrapper3() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<User>();
        updateWrapper.eq("name","向西").eq("age", 26).set("age",30);
        int rows = userMapper.update(null,updateWrapper);
        System.out.println("影响记录数："+rows);
    }

    /*
   更新方法
   传入where的实体类，更细数据
    */
    @Test
    public void updateByWrapper2() {
        User whereUser = new User();
        whereUser.setRealName("向北");
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<User>(whereUser);
        updateWrapper.eq("age", 26);
        User user = new User();
        user.setAge(28);
        user.setEmail("xb@baomidou.com");
        int rows = userMapper.update(user,updateWrapper);
        System.out.println("影响记录数："+rows);
    }

    /*
   更新方法
   通过wrapper更新数据
    */
    @Test
    public void updateByWrapper() {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<User>();
        updateWrapper.eq("name","向前").eq("age", 26);
        User user = new User();
        user.setAge(28);
        user.setEmail("lyw@baomidou.com");
        int rows = userMapper.update(user,updateWrapper);
        System.out.println("影响记录数："+rows);
    }

    /*
    更新方法
     */
    @Test
    public void updateById() {
        User user = new User();
//        user.setUserId(1497132526115696641L);
        user.setAge(26);
        user.setEmail("wxq@baomidou.com");
        int rows = userMapper.updateById(user);
        System.out.println("影响记录数："+rows);
    }
}
