package com.mp.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mp.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: UserMapper
 * @Description:
 * @date: 2022/2/25 16:02
 * @since JDK 1.8
 */
public interface UserMapper extends BaseMapper<User> {

//    @Select("select * from mp_user ${ew.customSqlSegment}")
    List<User> selectAll(@Param(Constants.WRAPPER)Wrapper<User> wrapper);

    IPage<User> selectUserPage(Page<User> page, @Param(Constants.WRAPPER)Wrapper<User> wrapper);
}
