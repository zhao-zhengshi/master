package com.mp.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: User
 * @Description:
 * @date: 2022/2/25 15:57
 * @since JDK 1.8
 */
@Data
@TableName("mp_user")
@EqualsAndHashCode(callSuper = false)
public class User extends Model<User> {

    private static final long serialVersionUID = 211496794190918715L;
    //主键
//    @TableId(type = IdType.ID_WORKER_STR)
    @TableId
    private String userId;
    //姓名
    @TableField(value = "name",condition = SqlCondition.LIKE)
    private String realName;
    //年龄
    @TableField(condition = "%s&lt;#{%s}")
    private Integer age;
    //邮箱
    private String email;
    //直属上级
    private Long managerId;
    //创建时间
    private LocalDateTime createTime;
    //备注
    @TableField(exist = false)
    private String remark;

}
