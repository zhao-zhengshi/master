package com.mp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mp.entity.User;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: UserService
 * @Description:
 * @date: 2022/3/1 15:32
 * @since JDK 1.8
 */
public interface UserService extends IService<User> {
}
