package com.mp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mp.dao.UserMapper;
import com.mp.entity.User;
import com.mp.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author zhaozhengshi
 * @version 1.0
 * @ClassName: UserServiceImpl
 * @Description:
 * @date: 2022/3/1 15:33
 * @since JDK 1.8
 */

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}
