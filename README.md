# MyBatis-Plus入门教程源码

#### 介绍
MyBatis-Plus入门教程源码
[慕课网视频](https://www.imooc.com/learn/1130)
## @视频主【老猿】
多年专注于Java开发，拥有丰富的项目开发经验，富有激情，热爱技术。是一名资深的Java老猿。 喜欢把好的技术分享给大家，讲课能结实际应用场景，深入浅出，语言风趣幽默，使同学们学习后能真正理解吸收。


#### 开发环境

intelliJ IDEA 2020.1

JDK 1.8

Git 2.34.0

Maven 3.8.3

Mysql 8.0

搭建好开发环境后执行mysql脚本初始化数据：\first\src\main\resources\数据库脚本.sql

#### 软件架构
后端技术：

spring-boot 2.6.4

mybatis-plus 3.1.0


#### 使用说明

1.  所有代码都是个人学习所敲源码
2.  部分mybatis-plus配置的使用仅仅学习，并未敲源码


